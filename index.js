const express = require('express')
const axios = require('axios')
const redis = require('redis')
const { response } = require('express')
const port = 8080
const REDIS_PORT = 6379
const client = redis.createClient(REDIS_PORT)
const app = express()


app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.append('Access-Control-Allow-Headers', 'Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, Referer, User-Agent');
    next();
  });


app.get('/metar1/ping', (req, res) => {
    res.json({
        data: 'pong'
    })
})

app.get('/metar1/info', cache, (req,res) => {
    if(req.query.scode){
        axios.get(`https://tgftp.nws.noaa.gov/data/observations/metar/stations/${req.query.scode}.TXT`)
        .then(function (response) {
            let data = response.data.replace(/\n/g, " ").trim().split(' ');
            let dataInfo = {
                station: data[2],
                last_observations: `${data[0]} at ${data[1]}`,
            }
            client.setex(req.query.scode, 300, JSON.stringify(dataInfo))
            res.json({
                data: dataInfo
            })
        })
        .catch(function (error) {
            console.log(error);
            res.json({
                error: error.message
            })
        })
    }
    else{
        res.json({
            data: "Please provide station code."
        })
    }
})

app.listen(port, () => console.log(`Metar http://localhost:${port}`))

function cache(req, res, next) {

    const scode = req.query.scode
  
    client.get(scode, (error, cachedData) => {
  
      if (error) throw error
  
      if (cachedData != null) {
  
        res.send({
            scode,
            cachedData
        })
  
      } else {
  
        next()
  
      }
  
    })
  
  }