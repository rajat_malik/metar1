# METAR

This node.js application is scraping data from a website and displaying it in a well-formatted manner.

## Getting Started

Clone the directory to your local machine. 
```
Install node
```
```
Install redis
```
```
npm Install in the root directory
```
```
Start redis server
```
```
node index.js
```

Your project is live and running.

## Authors

* **Rajat Malik** - *Initial work* - [rajatmalik21](https://github.com/rajatmalik21/)

